﻿using System;
using System.Data.Entity;
using Moq;
using NUnit.Framework;
using Web;

namespace Test
{
    public class ValidateEntityLoomTests
    {
        private FakeDbSet<Loom> _loomad = new FakeDbSet<Loom>();
        private AnimalsDbContext _sut;

        [SetUp]
        public void SetUp()
        {
            _loomad = new FakeDbSet<Loom>
            {
                new Loom
                {
                    Id = 1,
                    Species = "Koer",
                    Dob = DateTime.UtcNow,
                    Name = "Robi",
                    AddedOn = DateTime.UtcNow
                },
                new Loom
                {
                    Id = 2,
                    Species = "Kass",
                    Dob = DateTime.UtcNow,
                    Name = "Viki",
                    AddedOn = DateTime.UtcNow
                }
            };
            var mock = new Mock<AnimalsDbContext>();
            mock.Setup(x => x.Loom).Returns(_loomad);
            _sut = mock.Object;
        }

        [Test]
        public void WhenAdding_SameSpeciesAndName_ShouldError()
        {
            var message = _sut.GetValidationMessage(new Loom
                {
                    Species = "Koer",
                    Dob = DateTime.UtcNow,
                    Name = "Robi"
                },
                EntityState.Added);
            Assert.AreEqual("Sama liiki ja sama nimega looma ei saa mitmekordselt sisestada.", message);
        }

        [Test]
        public void WhenAdding_NoSpecies_ShouldError()
        {
            var message = _sut.GetValidationMessage(new Loom
                {
                    Dob = DateTime.UtcNow,
                    Name = "Robi"
                },
                EntityState.Added);
            Assert.AreEqual("Liik on kohustuslik väli.", message);
        }

        [Test]
        public void WhenAdding_NoName_ShouldError()
        {
            var message = _sut.GetValidationMessage(new Loom
                {
                    Species = "Koer",
                    Dob = DateTime.UtcNow
                },
                EntityState.Added);
            Assert.AreEqual("Nimi on kohustuslik väli.", message);
        }

        [Test]
        public void WhenAdding_NoDob_ShouldError()
        {
            var message = _sut.GetValidationMessage(new Loom
                {
                    Species = "Koer",
                    Name = "Robi"
                },
                EntityState.Added);
            Assert.AreEqual("Sünni aeg on kohustuslik väli.", message);
        }

        [Test]
        public void WhenChanging_SameSpeciesAndName_ShouldError()
        {
            var message = _sut.GetValidationMessage(new Loom
            {
                Species = "Koer",
                Dob = DateTime.UtcNow,
                Name = "Robi"
            },
                EntityState.Modified);
            Assert.AreEqual("Sama liiki ja sama nimega looma ei saa mitmekordselt sisestada.", message);
        }

        [Test]
        public void WhenChanging_NoSpecies_ShouldError()
        {
            var message = _sut.GetValidationMessage(new Loom
            {
                Dob = DateTime.UtcNow,
                Name = "Robi"
            },
                EntityState.Modified);
            Assert.AreEqual("Liik on kohustuslik väli.", message);
        }

        [Test]
        public void WhenChanging_NoName_ShouldError()
        {
            var message = _sut.GetValidationMessage(new Loom
            {
                Species = "Koer",
                Dob = DateTime.UtcNow
            },
                EntityState.Modified);
            Assert.AreEqual("Nimi on kohustuslik väli.", message);
        }

        [Test]
        public void WhenChanging_NoDob_ShouldError()
        {
            var message = _sut.GetValidationMessage(new Loom
            {
                Species = "Koer",
                Name = "Robi"
            },
                EntityState.Modified);
            Assert.AreEqual("Sünni aeg on kohustuslik väli.", message);
        }
    }
}
