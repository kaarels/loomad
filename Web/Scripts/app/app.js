﻿var app = angular.module('mainApp', []);

app.controller('animalCtrl', ['$scope', 'dataService', function($scope, dataService) {
    $scope.appTitle = "Loomad";
    $scope.addMode = false;
    $scope.newAnimal = {};
    $scope.species = [];
    $scope.animals = [];
    $scope.errorMessage = "";

    function viewformatAnimals() {
        $scope.animals.forEach(function (a) {
            a.dob = (new Date(a.dob));
            a.age = (new Date()).getFullYear() - a.dob.getFullYear();
        });
    }

    dataService.getSpecies().
        then(function(response) {
            $scope.species = response.data;
        }, function(response) {
            console.log(response.status + ": " + (response.data || 'no data'));
        });

    function init() {
        dataService.getAnimals().
            then(function (response) {
                $scope.animals = response.data;
                viewformatAnimals();
            }, function (response) {
                console.log(response.status + ": " + (response.data || 'no data'));
            });
    }

    init();

    $scope.getAverageAge = function() {
        var sum = 0;
        $scope.animals.forEach(function(a) { sum += a.age; });
        var avg = sum / $scope.animals.length;
        return avg > 0 ? avg : "";
    };

    $scope.deleteAnimal = function (animal) {
        if (confirm("Kas soovid kustutada?")) {
            dataService.deleteAnimal(animal.id);

            var index = $scope.animals.indexOf(animal);
            $scope.animals.splice(index, 1);
        }
    };

    $scope.editAnimal = function (animal) {
        animal.original = angular.toJson(animal);
        animal.editMode = true;
    };

    $scope.saveAnimal = function (animal) {
        if (confirm("Kas soovid muudatusi salvestada?")) {
            dataService.saveAnimal(animal)
               .then(function (response) {
                    $scope.errorMessage = "";
                }, function (response) {
                   if (response && response.data && response.data.message) {
                       $scope.errorMessage = response.data.message;
                       init();
                   }
                });

            animal.editMode = false;
            animal.original = {};
            viewformatAnimals();
        }
    };

    $scope.cancelEditAnimal = function (animal) {
        var original = angular.fromJson(animal.original);
        animal.species = original.species;
        animal.name = original.name;
        animal.dob = original.dob;
        animal.editMode = false;
    };

    $scope.addAnimal = function () {
        $scope.addMode = true;
    };

    $scope.cancelNew = function () {
        $scope.addMode = false;
        $scope.newAnimal = {};
    };

    $scope.addNewAnimal = function () {
        if (confirm("Kas lisada uus kirje?")) {
            dataService.addAnimal($scope.newAnimal).
                then(function (response) {
                    $scope.errorMessage = "";
                    $scope.animals.push(response.data);
                    viewformatAnimals();
                }, function(response) {
                    if (response && response.data && response.data.message) {
                        $scope.errorMessage = response.data.message;
                    }
                });
        }
        $scope.addMode = false;
        $scope.newAnimal = {};
    };
}]);