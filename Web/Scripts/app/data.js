﻿app.factory('dataService', function($http) {

    function getAnimals() {
        return $http({
            method: 'GET',
            url: 'api/animals',
            headers: { 'Content-Type': "application/json" }
        });
    }
    
    function getSpecies() {
        return $http({
            method: 'GET',
            url: 'api/species',
            headers: { 'Content-Type': "application/json" }
        });
    }

    function deleteAnimal(id) {
        return $http({
            method: 'DELETE',
            url: 'api/animals/' + id,
            headers: { 'Content-Type': "application/json" }
        });
    }

    function addAnimal(animal) {
        return $http({
            method: 'POST',
            url: 'api/animals',
            data: animal,
            headers: { 'Content-Type': "application/json" }
        });
    }

    function saveAnimal(animal) {
        return $http({
            method: 'PUT',
            url: 'api/animals',
            data: animal,
            headers: { 'Content-Type': "application/json" }
        });
    }

    var service = {
        getAnimals: getAnimals,
        getSpecies: getSpecies,
        deleteAnimal: deleteAnimal,
        addAnimal: addAnimal,
        saveAnimal: saveAnimal
    }

    return service;
});