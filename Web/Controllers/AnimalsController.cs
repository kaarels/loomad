﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Models;

namespace Web.Controllers
{
    [RoutePrefix("api")]
    public class AnimalsController : ApiController
    {
        [HttpGet]
        [Route("animals")]
        public IEnumerable<Animal> GetAnimals()
        {
            using (var context = new AnimalsDbContext())
            {
                var animals = context.Loom.ToList().Select(x => new Animal
                {
                    Id = x.Id,
                    Species = x.Species,
                    Name = x.Name,
                    Dob = x.Dob,
                    AddedOn = x.AddedOn
                });
                return animals;
            }
        }

        [HttpGet]
        [Route("species")]
        public IEnumerable<string> GetSpecies()
        {
            using (var context = new AnimalsDbContext())
            {
                var species = context.Liik.ToList().Select(x => x.Name);
                return species;
            }
        }

        [HttpPost]
        [Route("animals")]
        public HttpResponseMessage AddAnimal(Animal animal)
        {
            using (var context = new AnimalsDbContext())
            {
                var inserted = context.Loom.Add(new Loom
                {
                    Species = animal.Species,
                    Dob = animal.Dob,
                    Name = animal.Name,
                    AddedOn = DateTime.UtcNow
                });

                context.SaveChanges();

                animal.Id = inserted.Id;
                animal.AddedOn = inserted.AddedOn;
                return Request.CreateResponse(HttpStatusCode.Created, animal);
            }
        }

        [HttpPut]
        [Route("animals")]
        public HttpResponseMessage EditAnimal(Animal animal)
        {
            using (var context = new AnimalsDbContext())
            {
                var find = context.Loom.Find(animal.Id);
                find.Species = animal.Species;
                find.Dob = animal.Dob;
                find.Name = animal.Name;
                context.SaveChanges();
            }

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpDelete]
        [Route("animals/{id}")]
        public HttpResponseMessage DeleteAnimal(int id)
        {
            using (var context = new AnimalsDbContext())
            {
                context.Loom.Remove(context.Loom.Find(id));
                context.SaveChanges();
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }
    }
}