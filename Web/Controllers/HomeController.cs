﻿using System.Web.Mvc;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Animals");
        }

        public ActionResult Animals()
        {
            return View();
        }
    }
}