using System;

namespace Web.Models
{
    public class Animal
    {
        public int Id { get; set; }
        public string Species { get; set; }
        public string Name { get; set; }
        public DateTime Dob { get; set; }
        public DateTime AddedOn { get; set; }
    }
}