﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;

namespace Web
{
    public partial class AnimalsDbContext : DbContext
    {
        public string GetValidationMessage(Loom loom, EntityState state)
        {
            if (loom != null &&
                (state == EntityState.Added || state == EntityState.Modified))
            {
                if (loom.Dob == DateTime.MinValue)
                {
                    return "Sünni aeg on kohustuslik väli.";
                }

                if (loom.Dob.ToUniversalTime() > DateTime.UtcNow)
                {
                    return "Sünni aeg ei või olla tulevikus.";
                }

                if (string.IsNullOrEmpty(loom.Name))
                {
                    return "Nimi on kohustuslik väli.";
                }

                if (string.IsNullOrEmpty(loom.Species))
                {
                    return "Liik on kohustuslik väli.";
                }

                if (Loom.Any(x => x.Species == loom.Species && x.Name == loom.Name && x.Id != loom.Id))
                {
                    return "Sama liiki ja sama nimega looma ei saa mitmekordselt sisestada.";
                }
            }

            return null;
        }

        protected override DbEntityValidationResult ValidateEntity(DbEntityEntry entityEntry, IDictionary<object, object> items)
        {
            var message = GetValidationMessage(entityEntry.Entity as Loom, entityEntry.State);
            return message == null ? base.ValidateEntity(entityEntry, items) : ErrorResult(entityEntry, message);
        }

        private static DbEntityValidationResult ErrorResult(DbEntityEntry entityEntry, string message)
        {
            var list = new List<DbValidationError>
            {
                new DbValidationError("", message)
            };

            return new DbEntityValidationResult(entityEntry, list);
        }
    }
}