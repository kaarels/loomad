﻿namespace Web.Models
{
    public class Species
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}