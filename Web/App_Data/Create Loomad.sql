﻿USE [Loomad]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Liik] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
	CONSTRAINT [PK_Liik] PRIMARY KEY ([Id])
);

CREATE TABLE [dbo].[Loom] (
    [Id]      INT           IDENTITY (1, 1) NOT NULL,
    [Species] NVARCHAR (50) NOT NULL,
    [Name]    NVARCHAR (50) NOT NULL,
    [Dob]     DATETIME      NOT NULL,
    [AddedOn] DATETIME      NOT NULL, 
    CONSTRAINT [PK_Loom] PRIMARY KEY ([Id])
);


SET IDENTITY_INSERT [dbo].[Liik] ON
INSERT INTO [dbo].[Liik] ([Id], [Name]) VALUES (1, N'Kass')
INSERT INTO [dbo].[Liik] ([Id], [Name]) VALUES (2, N'Koer')
INSERT INTO [dbo].[Liik] ([Id], [Name]) VALUES (3, N'Kala')
INSERT INTO [dbo].[Liik] ([Id], [Name]) VALUES (4, N'Hamster')
SET IDENTITY_INSERT [dbo].[Liik] OFF

SET IDENTITY_INSERT [dbo].[Loom] ON
INSERT INTO [dbo].[Loom] ([Id], [Species], [Name], [Dob], [AddedOn]) VALUES (1, N'Koer', N'Robi', CAST(N'1991-01-01 00:00:00.000' AS DateTime), CAST(N'2016-12-18 12:09:36.000' AS DateTime))
INSERT INTO [dbo].[Loom] ([Id], [Species], [Name], [Dob], [AddedOn]) VALUES (2, N'Kass', N'Viki', CAST(N'1992-02-02 00:00:00.000' AS DateTime), CAST(N'2016-12-17 19:19:39.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Loom] OFF