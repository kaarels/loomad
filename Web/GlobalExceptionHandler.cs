﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace Web
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            var e = context.Exception as System.Data.Entity.Validation.DbEntityValidationException;
            if (e == null)
            {
                base.Handle(context);
                return;
            }

            var sb = new StringBuilder();
            foreach (var entityValidationError in e.EntityValidationErrors)
            {
                foreach (var validationError in entityValidationError.ValidationErrors)
                {
                    sb.Append(validationError.ErrorMessage);
                }
            }

            var response = context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, sb.ToString());
            context.Result = new ResponseMessageResult(response);
        }
    }
}